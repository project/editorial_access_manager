<?php

namespace Drupal\editorial_access_manager\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\editorial_access_manager\EditorialAccessManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides dynamic local tasks for editorial access management.
 */
class EditorialManagerLocalTasks extends DeriverBase implements ContainerDeriverInterface {
  use StringTranslationTrait;

  /**
   * The base plugin ID.
   *
   * @var string
   */
  protected $basePluginId;

  /**
   * Editorial access manager.
   *
   * @var \Drupal\editorial_access_manager\EditorialAccessManager
   */
  protected EditorialAccessManager $editorialAccessManager;

  /**
   * Constructs a new EditorialManagerLocalTasks.
   *
   * @param string $base_plugin_id
   *   The base plugin ID.
   * @param \Drupal\editorial_access_manager\EditorialAccessManager $editorial_access_manager
   *   The editorial access manager.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The translation manager.
   */
  public function __construct($base_plugin_id, EditorialAccessManager $editorial_access_manager, TranslationInterface $string_translation) {
    $this->basePluginId = $base_plugin_id;
    $this->editorialAccessManager = $editorial_access_manager;
    $this->stringTranslation = $string_translation;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $base_plugin_id,
      $container->get('editorial_access_manager.manager'),
      $container->get('string_translation')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    // Create tabs for all possible entity types.
    foreach ($this->editorialAccessManager->getSupportedEntityTypesList() as $entity_type_id) {
      // Find the route name for the translation overview.
      $translation_route_name = "entity.$entity_type_id.editorial_access_management";

      $base_route_name = "entity.$entity_type_id.canonical";
      $this->derivatives[$translation_route_name] = [
        'entity_type' => $entity_type_id,
        'title' => $this->t('Manage editorial access'),
        'route_name' => $translation_route_name,
        'base_route' => $base_route_name,
      ] + $base_plugin_definition;
    }
    return parent::getDerivativeDefinitions($base_plugin_definition);
  }

}
