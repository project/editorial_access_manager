<?php

namespace Drupal\editorial_access_manager\EventSubscriber;

use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\Core\Routing\RoutingEvents;
use Drupal\editorial_access_manager\EditorialAccessManager;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Route subscriber.
 */
class EditorialAccessManagerRouteSubscriber extends RouteSubscriberBase {

  /**
   * Used to create routes for supported entities.
   *
   * @var \Drupal\editorial_access_manager\EditorialAccessManager
   */
  protected EditorialAccessManager $manager;

  /**
   * Constructs the subscriber.
   *
   * @param \Drupal\editorial_access_manager\EditorialAccessManager $manager
   *   Editorial acccess manager service.
   */
  public function __construct(EditorialAccessManager $manager) {
    $this->manager = $manager;
  }

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    foreach ($this->manager->getSupportedEntityTypes() as $entity_type_id => $entity_type) {
      // Inherit admin route status from edit route, if exists.
      $is_admin = FALSE;
      $route_name = "entity.$entity_type_id.edit_form";
      if ($edit_route = $collection->get($route_name)) {
        $is_admin = (bool) $edit_route->getOption('_admin_route');
      }

      if ($entity_type->hasLinkTemplate('drupal:editorial-access-management')) {
        $route = new Route(
          $entity_type->getLinkTemplate('drupal:editorial-access-management'),
          [
            '_controller' => '\Drupal\editorial_access_manager\Controller\EditorialAccessManagerController::overview',
            'entity_type_id' => $entity_type_id,
          ],
          [
            '_entity_access' => $entity_type_id . '.view',
            '_editorial_access_manager_assign_access' => 'TRUE',
          ],
          [
            'parameters' => [
              $entity_type_id => [
                'type' => sprintf('entity:%s', $entity_type_id),
              ],
            ],
            '_admin_route' => $is_admin,
          ]
        );
        $route_name = "entity.$entity_type_id.editorial_access_management";
        $collection->add($route_name, $route);

        $translation_overview_route = $collection->get("entity.$entity_type_id.content_translation_overview");
        if ($translation_overview_route instanceof Route) {
          $translation_overview_route->setDefault('_controller', '\Drupal\editorial_access_manager\Controller\ContentTranslationController::overview');
        }
      }
    }

  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = parent::getSubscribedEvents();

    $events[RoutingEvents::ALTER] = ['onAlterRoutes', -219];

    return $events;
  }

}
