<?php

namespace Drupal\editorial_access_manager;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Creates all dynamic permissions related with existing content entity types.
 */
final class EditorialAccessManagerPermissions implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * Gets all entity types supported to generate specific permissions.
   *
   * @var \Drupal\editorial_access_manager\EditorialAccessManager
   */
  protected EditorialAccessManager $manager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    /** @var \Drupal\editorial_access_manager\EditorialAccessManager $manager */
    $manager = $container->get('editorial_access_manager.manager');
    return new static($manager);
  }

  /**
   * Constructs the manager.
   *
   * @param EditorialAccessManager $manager
   *   Manager.
   */
  public function __construct(EditorialAccessManager $manager) {
    $this->manager = $manager;
  }

  /**
   * Callback tgo get all dynamic permissions.
   */
  public function permissions() {
    $permissions = [];
    /**
     * @var string $entity_type_id
     * @var \Drupal\Core\Entity\ContentEntityTypeInterface $entity_type
     */
    foreach ($this->manager->getSupportedEntityTypes() as $entity_type_id => $entity_type) {
      $args = ['@entity_type' => $entity_type->getLabel()];
      $permissions[sprintf('assign %s edition', $entity_type_id)] = [
        'title' => $this->t('Assign @entity_type edition', $args),
        'description' => $this->t('Allows to assign the @entity_type edition to assignable users.', $args),
      ];
      $permissions[sprintf('assign %s translation', $entity_type_id)] = [
        'title' => $this->t('Assign @entity_type translation', $args),
        'description' => $this->t('Allows to assign the @entity_type translation to assignable users.', $args),
      ];
      $permissions[sprintf('edit assigned %s', $entity_type_id)] = [
        'title' => $this->t('Edit assigned @entity_type', $args),
        'description' => $this->t('Allows to either edit or translate assigned @entity_type.', $args),
      ];
    }
    return $permissions;
  }

}
