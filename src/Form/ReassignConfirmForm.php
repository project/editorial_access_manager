<?php

namespace Drupal\editorial_access_manager\Form;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\editorial_access_manager\EditorialAccessManager;
use Drupal\node\NodeInterface;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form to confirm that the content will be reassigned from one user to another.
 *
 * The person who does the reassignment know the affected content.
 */
class ReassignConfirmForm extends ConfirmFormBase {

  /**
   * Used to reassign the content.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected Connection $database;

  /**
   * Used to load content and show the resume.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Used to recalculate node grants.
   *
   * @var \Drupal\editorial_access_manager\EditorialAccessManager
   */
  protected EditorialAccessManager $editorialAccessManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    /** @var \Drupal\Core\Database\Connection $database */
    $database = $container->get('database');
    $instance->database = $database;
    /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager */
    $entity_type_manager = $container->get('entity_type.manager');
    $instance->entityTypeManager = $entity_type_manager;

    /** @var \Drupal\editorial_access_manager\EditorialAccessManager $editorial_access_manager */
    $editorial_access_manager = \Drupal::service('editorial_access_manager.manager');
    $instance->editorialAccessManager = $editorial_access_manager;
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'editorial_access_manager_reassign_confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    /** @var \Drupal\user\UserInterface $assignee_old */
    $assignee_old = $this->getRouteMatch()->getParameter('assignee_old');
    /** @var \Drupal\user\UserInterface $assignee_new */
    $assignee_new = $this->getRouteMatch()->getParameter('assignee_new');
    return $this->t('Are you sure you want to do reassign content from :assignee_old to :assignee_new?', [
      ':assignee_old' => $assignee_old->getDisplayName(),
      ':assignee_new' => $assignee_new->getDisplayName(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form_built = parent::buildForm($form, $form_state);
    /** @var \Drupal\user\UserInterface $assignee_old */
    $assignee_old = $this->getRouteMatch()->getParameter('assignee_old');
    /** @var \Drupal\user\UserInterface $assignee_new */
    $assignee_new = $this->getRouteMatch()->getParameter('assignee_new');
    $form_built['resume'] = $this->buildResume($assignee_old, $assignee_new);
    return $form_built;
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('editorial_access_manager.reassign');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\user\UserInterface $assignee_old */
    $assignee_old = $this->getRouteMatch()->getParameter('assignee_old');
    /** @var \Drupal\user\UserInterface $assignee_new */
    $assignee_new = $this->getRouteMatch()->getParameter('assignee_new');
    if ($this->reassignContent($assignee_old, $assignee_new)) {
      $form_state->setRedirect('editorial_access_manager.reassign');
      $this->messenger()->addStatus($this->t('All content has been reassigned from :assignee_old to :assignee_new', [
        ':assignee_old' => $assignee_old->getDisplayName(),
        ':assignee_new' => $assignee_new->getDisplayName(),
      ]));
    }
    else {
      $form_state->setError($form, $this->t('There was a problem reassigning the content, please check the recent log messages.'));
    }
  }

  /**
   * Build a table that resume which content will be reassigned.
   *
   * @param \Drupal\user\UserInterface $assignee_old
   *   Old assignee.
   * @param \Drupal\user\UserInterface $assignee_new
   *   New assignee.
   *
   * @return array
   *   Render array that shows all the affected content.
   */
  protected function buildResume(UserInterface $assignee_old, UserInterface $assignee_new) {
    $assignee_old_content_query = $this
      ->database
      ->select('editorial_access', 'ea');
    $assignee_old_content_query->addField('ea', 'entity_type');
    $assignee_old_content_query->addExpression('COUNT(*)', 'total');
    $assignee_old_content_query->condition('uid', (string) $assignee_old->id());
    $assignee_old_content_query->groupBy('entity_type');
    /** @var \Drupal\Core\Database\StatementInterface $assignee_old_content_query_result */
    $assignee_old_content_query_result = $assignee_old_content_query
      ->execute();

    $table_rows = [];
    foreach ($assignee_old_content_query_result->fetchAll() as $row) {
      /**
       * @var object $row
       */
      if (!empty($row->entity_type) && !empty($row->total)) {
        $entity_type = $this->entityTypeManager->getDefinition($row->entity_type);
        if ($entity_type instanceof EntityTypeInterface) {
          $table_rows[] = [
            $entity_type->getLabel(),
            $row->total,
          ];
        }
      }
    }

    return [
      '#weight' => -1,
      '#type' => 'container',
      'title' => [
        '#type' => 'html_tag',
        '#tag'  => 'p',
        '#value' => $this->t('Resume of entities that will be assigned to :assignee_new:', [
          ':assignee_new' => $assignee_new->getDisplayName(),
        ]),
      ],
      'table' => [
        '#theme' => 'table',
        '#header' => [
          $this->t('Entity type'),
          $this->t('Total'),
        ],
        '#rows' => $table_rows,
      ],
    ];
  }

  /**
   * Reassign all content from old assignee to new assignee.
   *
   * @param \Drupal\user\UserInterface $assignee_old
   *   Old assignee.
   * @param \Drupal\user\UserInterface $assignee_new
   *   New assignee.
   *
   * @return bool
   *   TRUE when the reassignment is okay, FALSE when there are problems.
   */
  protected function reassignContent(UserInterface $assignee_old, UserInterface $assignee_new) {
    $transaction = $this->database->startTransaction('editorial_access_manager_reassign');
    try {

      /** @var \Drupal\Core\Database\StatementInterface $assignee_old_content_query_result */
      $assignee_old_content_query_result = $this
        ->database
        ->select('editorial_access', 'ea')
        ->fields('ea')
        ->condition('uid', (string) $assignee_old->id())
        ->execute();
      $assignee_old_content = $assignee_old_content_query_result->fetchAll(\PDO::FETCH_ASSOC);

      $this
        ->database
        ->delete('editorial_access')
        ->condition('uid', (string) $assignee_old->id())
        ->execute();

      foreach ($assignee_old_content as $content) {
        $assignee_new_content_row = $content;
        $assignee_new_content_row['date'] = time();
        $assignee_new_content_row['uid'] = $assignee_new->id();
        $merge_query = $this
          ->database
          ->merge('editorial_access')
          ->keys([
            'uid' => $assignee_new->id(),
            'entity_type' => $assignee_new_content_row['entity_type'],
            'entity_id' => $assignee_new_content_row['entity_id'],
            'langcode' => $assignee_new_content_row['langcode'],
          ]);

        $merge_query->fields($assignee_new_content_row);
        $merge_query->execute();
      }

      $nodes = array_filter($assignee_old_content, function ($row) {
        return $row['entity_type'] == 'node';
      });

      if (!empty($nodes)) {
        $this->generateNodeAccessRebuildBatch(array_column($nodes, 'entity_id'));
      }
    }
    catch (\Exception $e) {
      watchdog_exception(
        'editorial_access_manager',
        $e,
        sprintf('There was an error trying to reassign editorial access from %s to %s', $assignee_old->getDisplayName(), $assignee_new->getDisplayName()),
      );
      $transaction->rollBack();
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Generate batch to recalculate recalculated node's access.
   *
   * @param array $nids
   *   List of node ids that were affected by the recalculation.
   */
  protected function generateNodeAccessRebuildBatch(array $nids) {
    $operations = array_map(function ($nid) {
      return [
        [$this, 'recalculateNodeGrants'],
        [
          $nid,
        ],
      ];
    }, $nids);
    $batch = [
      'title' => $this->t('Recalculating affected node permissions'),
      'operations' => $operations,
    ];
    batch_set($batch);
  }

  /**
   * Recalculate node grants for a specific node id.
   *
   * @param int $nid
   *   Nid which grants will be recalculated.
   */
  public function recalculateNodeGrants($nid) {
    $node = $this->entityTypeManager->getStorage('node')->load($nid);
    if ($node instanceof NodeInterface) {
      $this->editorialAccessManager->recalculateNodeGrants($node);
    }
  }

}
