<?php

namespace Drupal\editorial_access_manager\Controller;

use Drupal\content_translation\Controller\ContentTranslationController as OriginalContentTranslationController;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\editorial_access_manager\EditorialAccessManager;
use Drupal\editorial_access_manager\EditorialAccessManagerTranslationHandler;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Overrides content translation to provide granular translation creation links.
 */
class ContentTranslationController extends OriginalContentTranslationController {

  use LangcodeDetectionTrait;

  /**
   * Used to create routes for supported entities.
   *
   * @var \Drupal\editorial_access_manager\EditorialAccessManager
   */
  protected EditorialAccessManager $editorialAccessManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    /** @var \Drupal\editorial_access_manager\EditorialAccessManager $access_manager */
    $access_manager = $container->get('editorial_access_manager.manager');
    $instance->editorialAccessManager = $access_manager;
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function overview(RouteMatchInterface $route_match, $entity_type_id = NULL) {
    $build = parent::overview($route_match, $entity_type_id);
    if (empty($entity_type_id)) {
      return [];
    }
    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = $route_match->getParameter($entity_type_id);

    if (!$this->editorialAccessManager->userIsAssignable($entity, $this->currentUser)) {
      return $build;
    }

    foreach ($build['content_translation_overview']['#rows'] as &$row) {
      $language = $this->detectLanguage($row[0], $entity);
      /** @var \Drupal\editorial_access_manager\EditorialAccessManagerTranslationHandler $translation_handler */
      $translation_handler = $this->entityTypeManager->getHandler($entity->getEntityTypeId(), 'translation');
      foreach ($row as &$row_item) {
        if ($entity->isTranslatable() && is_array($row_item) &&  !empty($row_item['data']['#type'])
          && $row_item['data']['#type'] == 'operations') {
          if ($this->linksMissingForTranslationAdd($entity, $language, $row_item)) {

            $options = ['language' => $language];
            $add_url = $entity->toUrl('drupal:content-translation-add', $options)
              ->setRouteParameter('source', $entity->getUntranslated()->language()->getId())
              ->setRouteParameter('target', $language->getId());
            $row_item['data']['#links']['add'] = [
              'title' => $this->t('Add'),
              'url' => $add_url,
            ];
          }
          elseif ($this->linksMissingForTranslationEdit($entity, $language, $row_item)) {
            $options = ['language' => $language];
            $edit_url = $entity->toUrl('drupal:content-translation-edit', $options)
              ->setRouteParameter('language', $language->getId());
            $row_item['data']['#links']['add'] = [
              'title' => $this->t('Edit'),
              'url' => $edit_url,
            ];
          }
          // Due to how base class gives access to edit links,
          // it is needed to restrict access after
          // when the user does not have real access.
          elseif (!empty($row_item['data']['#links'])
            && !$this->editorialAccessManager->getTranslationAccess($entity, $this->currentUser, $language)) {
            $row_item['data']['#links'] = $this->getLinksByRestrictedAccess($translation_handler, $entity, $row_item['data']['#links']);
          }
        }
      }
    }
    return $build;
  }

  /**
   * Check links are missing for adding a translation.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   Entity.
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   Language.
   * @param array $row_item
   *   Row item.
   *
   * @return bool
   *   TRUE if links are missing.
   */
  protected function linksMissingForTranslationAdd(ContentEntityInterface $entity, LanguageInterface $language, array $row_item) {
    return !$entity->hasTranslation($language->getId()) && empty($row_item['data']['#links'])
      && $this->editorialAccessManager->getTranslationAccess($entity, $this->currentUser, $language);
  }

  /**
   * Check links are missing for editing a translation.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   Entity.
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   Language.
   * @param array $row_item
   *   Row item.
   *
   * @return bool
   *   TRUE if links are missing.
   */
  protected function linksMissingForTranslationEdit(ContentEntityInterface $entity, LanguageInterface $language, array $row_item) {
    return $entity->hasTranslation($language->getId()) && empty($row_item['data']['#links'])
      && $this->editorialAccessManager->getTranslationAccess($entity, $this->currentUser, $language);
  }

  /**
   * Restricts links access.
   *
   * This is needed because the base class is adding links
   * even if it is not possible to edit the entity at a specific language.
   *
   * @param \Drupal\editorial_access_manager\EditorialAccessManagerTranslationHandler $translation_handler
   *   Translation handler.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity it is needed to restrict access.
   * @param array $links
   *   Links that were added by the base class.
   *
   * @return array
   *   Allowed links.
   */
  protected function getLinksByRestrictedAccess(EditorialAccessManagerTranslationHandler $translation_handler, EntityInterface $entity, array $links) {
    foreach (array_keys($links) as $link_key) {
      switch ($link_key) {
        case 'edit':
          $op = 'update';
          break;

        case 'add':
          $op = 'create';
          break;

        default:
          $op = $link_key;

      }
      if (!$translation_handler->getBaseAccess($entity, $op)->isAllowed()) {
        unset($links[$link_key]);
      }
    }

    return $links;
  }

}
