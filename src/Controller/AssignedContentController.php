<?php

namespace Drupal\editorial_access_manager\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Link;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Page where user sees its own assigned content.
 */
class AssignedContentController extends ControllerBase {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected AccountInterface $account;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected Connection $connection;

  /**
   * The controller constructor.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   */
  public function __construct(Connection $connection) {
    $this->connection = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database')
    );
  }

  /**
   * Builds the response.
   */
  public function build() {

    $build = [];

    $assigned_content_list = $this->getAssignedContent();
    if (!empty($assigned_content_list)) {
      $headers = [
        $this->t('Entity type'),
        $this->t('Bundle'),
        $this->t('Entity'),
        $this->t('Language'),
        $this->t('Link'),
      ];

      $rows = [];

      foreach ($assigned_content_list as $assigned_content) {
        $entity = $this->entityTypeManager()->getStorage($assigned_content->entity_type)->load($assigned_content->entity_id);
        $language = $this->languageManager()->getLanguage($assigned_content->langcode);
        if ($entity instanceof ContentEntityInterface
          && ($this->currentUser()->hasPermission('edit assigned entity')
            || $this->currentUser()->hasPermission(sprintf('edit assigned %s', $entity->getEntityTypeId())))
          && $language instanceof LanguageInterface) {
          $rows[] = [
            $entity->getEntityType()->getLabel(),
            $this->getEntityBundleLabel($entity),
            $this->getEntityViewLink($entity, $language),
            $language->getName(),
            $this->getEntityEditOrCreateLink($entity, $language),
          ];
        }
      }

      $build['assigned_content_table'] = [
        '#theme' => 'table',
        '#header' => $headers,
        '#rows' => $rows,
      ];
    }
    else {
      $build['assigned_content_table'] = [
        '#markup' => $this->t('There is no assigned content yet.'),
      ];

    }

    return $build;
  }

  /**
   * Get the entity bundle label.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   Entity.
   *
   * @return string
   *   Entity bundle label.
   */
  protected function getEntityBundleLabel(ContentEntityInterface $entity) {
    $bundle_entity_type = $entity->getEntityType()->getBundleEntityType();
    $bundle = $entity->bundle();
    if (!empty($bundle) && !empty($bundle_entity_type)) {
      $bundle_entity = $this->entityTypeManager()->getStorage($bundle_entity_type)->load($bundle);
      return $bundle_entity instanceof EntityInterface ? (string) $bundle_entity->label() : '';
    }
    return '';
  }

  /**
   * Get the link to view the entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   Entity.
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   Language the view is wanted to be seen, if available.
   *
   * @return \Drupal\Core\Link
   *   Link to view the entity.
   */
  protected function getEntityViewLink(ContentEntityInterface $entity, LanguageInterface $language) {
    if ($entity->language()->getId() != $language->getId() && $entity->hasTranslation($language->getId())) {
      return $entity->getTranslation($language->getId())->toLink();
    }
    return $entity->toLink();
  }

  /**
   * Build a link to edit , or a link to create the translation.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   Entity.
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   Language.
   *
   * @return \Drupal\Core\Link
   *   Link.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  protected function getEntityEditOrCreateLink(ContentEntityInterface $entity, LanguageInterface $language) {
    if ($entity->language()->getId() != $language->getId() && $entity->hasTranslation($language->getId())) {
      return $entity->getTranslation($language->getId())->toLink('Edit translation', 'edit-form');
    }
    elseif ($entity->language()->getId() != $language->getId()) {
      $url = $entity->toUrl('drupal:content-translation-add', ['language' => $this->languageManager->getCurrentLanguage()])
        ->setRouteParameter('source', $entity->getUntranslated()->language()->getId())
        ->setRouteParameter('target', $language->getId());
      return Link::fromTextAndUrl('Create translation', $url);
    }
    return $entity->toLink('Edit', 'edit-form');
  }

  /**
   * Gets the assigned content from the database.
   *
   * @return array
   *   List of assigned content.
   */
  protected function getAssignedContent() {
    /** @var \Drupal\Core\Database\StatementInterface $query_result */
    $query_result = $this->connection->select('editorial_access', 'ea')
      ->fields('ea')
      ->condition('uid', (string) $this->currentUser()->id())
      ->orderBy('date', 'DESC')
      ->orderBy('entity_id', 'DESC')
      ->orderBy('entity_type')
      ->execute();
    return $query_result->fetchAll();
  }

}
