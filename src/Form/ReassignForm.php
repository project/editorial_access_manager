<?php

namespace Drupal\editorial_access_manager\Form;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Editorial access manager form.
 */
class ReassignForm extends FormBase {

  /**
   * Used to get old assignee entity types.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected Connection $database;

  /**
   * Used to ensure users exist and have permissions.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);

    /** @var \Drupal\Core\Database\Connection $database */
    $database = $container->get('database');
    $instance->database = $database;

    /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager */
    $entity_type_manager = $container->get('entity_type.manager');
    $instance->entityTypeManager = $entity_type_manager;
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'editorial_access_manager_reassign';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['assignee_old'] = [
      '#title' => $this->t('Old assignee'),
      '#type' => 'entity_autocomplete',
      '#target_type' => 'user',
      '#selection_handler' => 'editorial_access_manager',
      '#required' => TRUE,
    ];

    $form['assignee_new'] = [
      '#title' => $this->t('New assignee'),
      '#type' => 'entity_autocomplete',
      '#target_type' => 'user',
      '#selection_handler' => 'editorial_access_manager',
      '#required' => TRUE,
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Reassign'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->getValue('assignee_old') === $form_state->getValue('assignee_new')) {
      $form_state->setError($form['assignee_new'], 'Assignees are the same user, please choose different new and old assignees.');
    }

    $user_storage = $this->entityTypeManager->getStorage('user');
    $assignee_old = $user_storage->load($form_state->getValue('assignee_old'));
    if (!$assignee_old instanceof UserInterface) {
      $form_state->setError($form['assignee_old'], 'Old assignee does not exists.');
    }

    $assignee_new = $user_storage->load($form_state->getValue('assignee_new'));
    if (!$assignee_new instanceof UserInterface) {
      $form_state->setError($form['assignee_new'], 'New assignee does not exists.');
    }

    if ($assignee_old instanceof UserInterface && $assignee_new instanceof UserInterface) {
      $assignee_old_entity_types = $this->getUserAssignedEntityTypes($assignee_old);
      if (!empty($assignee_old_entity_types) && !$this->newAssigneeCanEditOldAssigneeEntityTypes($assignee_new, $assignee_old_entity_types)) {
        $form_state->setError(
          $form['assignee_old'],
          'New assignee is not able to edit all the content the old assignee has assigned. Please review the new assignee roles / permissions.',
        );
      }
      elseif (empty($assignee_old_entity_types)) {
        $form_state->setError($form['assignee_old'], 'Old assignee does not have assigned content.');
      }
    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('editorial_access_manager.reassign_confirm', [
      'assignee_new' => $form_state->getValue('assignee_new'),
      'assignee_old' => $form_state->getValue('assignee_old'),
    ]);
  }

  /**
   * Ensure new assignee is able to edit old assignee content.
   *
   * @param \Drupal\user\UserInterface $user
   *   New assignee.
   * @param array $entity_types
   *   Entity types.
   *
   * @return bool
   *   TRUE if new assignee has permissions to edit old assignee content.
   */
  protected function newAssigneeCanEditOldAssigneeEntityTypes(UserInterface $user, array $entity_types) {
    if ($user->hasPermission('edit assigned entity')) {
      return TRUE;
    }

    foreach ($entity_types as $entity_type) {
      if (!$user->hasPermission(sprintf('edit assigned %s', $entity_type))) {
        return FALSE;
      }
    }
    return TRUE;
  }

  /**
   * Get the list of entity types the user has assigned.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user.
   *
   * @return array<string>
   *   List of entity type IDs.
   */
  protected function getUserAssignedEntityTypes(UserInterface $user) {
    /** @var \Drupal\Core\Database\StatementInterface $query_result */
    $query_result = $this->database->select('editorial_access', 'ea')
      ->fields('ea', ['entity_type'])
      ->distinct()
      ->condition('uid', (string) $user->id())
      ->execute();

    return array_values($query_result->fetchAllKeyed(0, 0));
  }

}
