<?php

namespace Drupal\editorial_access_manager;

use Drupal\content_translation\ContentTranslationHandler as OriginalContentTranslationHandler;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Handle access to assigned content.
 */
class EditorialAccessManagerTranslationHandler extends OriginalContentTranslationHandler {

  /**
   * Used to check translation access.
   *
   * @var EditorialAccessManager
   */
  protected EditorialAccessManager $editorialAccessManager;

  /**
   * Used to get language from source and target.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected RouteMatchInterface $routeMatch;

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    $instance = parent::createInstance($container, $entity_type);
    /** @var \Drupal\editorial_access_manager\EditorialAccessManager $editorial_access_manager */
    $editorial_access_manager = $container->get('editorial_access_manager.manager');
    $instance->editorialAccessManager = $editorial_access_manager;
    /** @var \Drupal\Core\Routing\RouteMatchInterface $route_match */
    $route_match = $container->get('current_route_match');
    $instance->routeMatch = $route_match;
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getTranslationAccess(EntityInterface $entity, $op) {
    if (in_array($op, ['view', 'create', 'update']) && $this->editorialAccessManager->getTranslationAccess($entity, $this->currentUser, $this->getTranslationLanguage($entity))) {
      return AccessResult::allowed();
    }
    return parent::getTranslationAccess($entity, $op);
  }

  /**
   * Get translation access but using the logic of content translation module.
   */
  public function getBaseAccess($entity, $op) {
    return parent::getTranslationAccess($entity, $op);
  }

  /**
   * Get the language from context or entity.
   *
   * Tries to get the language from source and target parameters added
   * in a translation route.
   *
   * @return \Drupal\Core\Language\LanguageInterface
   *   Language found.
   */
  protected function getTranslationLanguage(EntityInterface $entity) {
    $source = $this->routeMatch->getParameter('source');
    $target = $this->routeMatch->getParameter('target');
    if ($source instanceof LanguageInterface && $target instanceof LanguageInterface) {
      return $target;
    }
    return $entity->language();
  }

}
