<?php

namespace Drupal\editorial_access_manager\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\editorial_access_manager\EditorialAccessManager;
use Drupal\node\NodeInterface;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Editorial access manager form.
 */
class EditorialAssignmentForm extends FormBase {

  /**
   * Used to save editorial access.
   *
   * @var \Drupal\editorial_access_manager\EditorialAccessManager
   */
  protected EditorialAccessManager $manager;

  /**
   * Used to build the form page title.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected LanguageManagerInterface $languageManager;

  /**
   * Used acquire node grants.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);

    /** @var \Drupal\editorial_access_manager\EditorialAccessManager $manager */
    $manager = $container->get('editorial_access_manager.manager');
    $instance->manager = $manager;

    /** @var \Drupal\Core\Language\LanguageManagerInterface $language_manager */
    $language_manager = $container->get('language_manager');
    $instance->languageManager = $language_manager;

    /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager */
    $entity_type_manager = $container->get('entity_type.manager');
    $instance->entityTypeManager = $entity_type_manager;

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'editorial_access_manager_editorial_assignment';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    if (!$form_state->has('additional_assignees')) {
      $form_state->set('additional_assignees', 0);
    }

    $additional_assignees = $form_state->get('additional_assignees');

    if ($form_state->has('entity')) {
      $entity = $form_state->get('entity');
    }
    else {
      $entity = $this->manager->getEntityFromRouteParameters();
      $form_state->set('entity', $entity);
      $form_state->set('langcode', $this->getRouteMatch()->getParameter('langcode'));
    }

    $entity_type = $entity->getEntityTypeId();

    $current_assignees = $this->manager->getEntityAssigneesPerLanguage($entity, $form_state->get('langcode'));

    /** @var \Drupal\Core\Language\LanguageInterface $language */
    $language = $this->languageManager->getLanguage($form_state->get('langcode'));

    $form['#title'] = sprintf('Assignee selection for %s language', $language->getName());

    $form['users_selection'] = [
      '#type' => 'fieldset',
      '#prefix' => "<div id='editorial-manager-access-selection'>",
      '#title' => $this->t('Assignees'),
      '#suffix' => "</div>",
    ];

    /** @var array<int, array>|array<string, bool> $users_widget */
    $users_widget = [];

    if (!empty($current_assignees)) {
      foreach ($current_assignees as $current_assignee) {
        $users_widget[] = $this->entityAutocompleteElement($entity_type, $current_assignee);
      }
    }
    else {
      $users_widget[] = $this->entityAutocompleteElement($entity_type);
    }

    $users_widget['#tree'] = TRUE;

    for ($i = 0; $i < $additional_assignees; $i++) {
      $users_widget[] = $this->entityAutocompleteElement($entity_type);
    }

    $form['users_selection']['users'] = $users_widget;

    $form['users_selection']['add_more'] = [
      '#type' => 'submit',
      '#ajax' => [
        'callback' => '::addMoreUsersAjaxCallback',
        'wrapper' => 'editorial-manager-access-selection',
      ],
      '#submit' => ['::addMoreUsers'],
      '#value' => $this->t('Add more'),
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    return $form;
  }

  /**
   * Submit handler for the "add-one-more" button.
   *
   * Increments the max counter and causes a rebuild.
   */
  public function addMoreUsers(array &$form, FormStateInterface $form_state) {
    $additional_assignees = $form_state->get('additional_assignees');
    $form_state->set('additional_assignees', $additional_assignees + 1);
    $form_state->setRebuild();
  }

  /**
   * Ajax callback to replace the user selection widget.
   *
   * It is used to show the additional users in the form.
   */
  public static function addMoreUsersAjaxCallback(array &$form, FormStateInterface $formState) {
    return $form['users_selection'];
  }

  /**
   * Generates autocomplete for assignable editors.
   *
   * @param string $entity_type
   *   Entity type that the user will be assigned to.
   * @param \Drupal\user\UserInterface|null $default_value
   *   Default value.
   *
   * @return array
   *   Form element.
   */
  protected function entityAutocompleteElement($entity_type, UserInterface $default_value = NULL) {
    return [
      '#type' => 'entity_autocomplete',
      // We can specify entity types to autocomplete.
      '#target_type' => 'user',
      '#selection_handler' => 'editorial_access_manager',
      '#selection_settings' => [
        'editorial_content_entity_type' => $entity_type,
      ],
      '#default_value' => $default_value,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\Core\Entity\EntityInterface $entity */
    $entity = $form_state->get('entity');
    $users = array_filter($form_state->getValue('users'));
    if (!empty($users)) {
      $this->manager->setEntityAssignees($entity, (string) $form_state->get('langcode'), $users);
    }
    else {
      $this->manager->clearEntityAssignees($entity, (string) $form_state->get('langcode'));
    }

    // Recalculate grants for nodes.
    if ($entity instanceof NodeInterface) {
      $this->manager->recalculateNodeGrants($entity);
    }

  }

}
