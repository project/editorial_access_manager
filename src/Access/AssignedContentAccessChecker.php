<?php

namespace Drupal\editorial_access_manager\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\editorial_access_manager\EditorialAccessManager;

/**
 * Checks access to assigned content pages.
 */
class AssignedContentAccessChecker implements AccessInterface {

  /**
   * Used to get the list of entity types supported.
   *
   * @var \Drupal\editorial_access_manager\EditorialAccessManager
   */
  protected EditorialAccessManager $manager;

  /**
   * Constructs the access checker.
   *
   * @param \Drupal\editorial_access_manager\EditorialAccessManager $manager
   *   Editorial access manager.
   */
  public function __construct(EditorialAccessManager $manager) {
    $this->manager = $manager;
  }

  /**
   * Check access to editorial manager assignment page.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Account.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(AccountInterface $account) {
    $permissions = [
      'edit assigned entity',
    ];

    foreach ($this->manager->getSupportedEntityTypesList() as $entity_type) {
      $permissions[] = sprintf('edit assigned %s', $entity_type);
    }

    return AccessResult::allowedIfHasPermissions($account, $permissions, 'OR');
  }

}
