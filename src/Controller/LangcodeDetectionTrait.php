<?php

namespace Drupal\editorial_access_manager\Controller;

use Drupal\Core\Entity\TranslatableInterface;

/**
 * Provides methods to detect language by translation overview label.
 */
trait LangcodeDetectionTrait {

  /**
   * Detect a langcode by its label.
   *
   * @param string $label
   *   Label of the langcode.
   * @param \Drupal\Core\Entity\TranslatableInterface $entity
   *   Entity.
   *
   * @return \Drupal\Core\Language\LanguageInterface
   *   Language obtained, default entity main language.
   */
  protected function detectLanguage(string $label, TranslatableInterface $entity) {
    foreach ($this->languageManager()->getLanguages() as $language) {
      if ($language->getName() == $label) {
        return $language;
      }
    }
    // Default language is returned assuming the label not found
    // is the one it says "(Original language)".
    return $entity->getUntranslated()->language();
  }

}
