<?php

namespace Drupal\editorial_access_manager\Plugin\EntityReferenceSelection;

use Drupal\editorial_access_manager\EditorialAccessManager;
use Drupal\user\Plugin\EntityReferenceSelection\UserSelection;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides selection for assignable users.
 *
 * @EntityReferenceSelection(
 *   id = "editorial_access_manager_assignable_user",
 *   label = @Translation("Assignable user"),
 *   entity_types = {"user"},
 *   group = "editorial_access_manager",
 *   weight = 1
 * )
 */
class AssignableUserSelection extends UserSelection {

  /**
   * Used to get supported entity types.
   *
   * @var \Drupal\editorial_access_manager\EditorialAccessManager
   */
  protected EditorialAccessManager $editorialAccessManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    /** @var \Drupal\editorial_access_manager\EditorialAccessManager $editorial_access_manager */
    $editorial_access_manager = $container->get('editorial_access_manager.manager');
    $instance->editorialAccessManager = $editorial_access_manager;
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function buildEntityQuery($match = NULL, $match_operator = 'CONTAINS') {
    $query = parent::buildEntityQuery($match, $match_operator);

    // Always filter by roles because in other case it may lead to
    // don't adding the filter and showing all users even they do not
    // have the permission.
    $roles = $this->getRolesWithAssignmentPermissions();
    $query->condition('roles', $roles, 'IN');

    return $query;
  }

  /**
   * Get roles that can edit assigned entities.
   *
   * Used to be able to query users with this permission,
   * if they have the role then they have the permission.
   *
   * @return string[]
   *   Role IDs.
   */
  protected function getRolesWithAssignmentPermissions() {
    $roles = user_role_names(FALSE, 'edit assigned entity');
    $configuration = $this->getConfiguration();
    if (!empty($configuration['editorial_content_entity_type'])) {
      $roles += user_role_names(FALSE, sprintf('edit assigned %s', $configuration['editorial_content_entity_type']));
    }
    return array_keys($roles);
  }

}
