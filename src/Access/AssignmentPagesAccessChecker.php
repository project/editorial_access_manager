<?php

namespace Drupal\editorial_access_manager\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Config\Entity\ThirdPartySettingsInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\editorial_access_manager\EditorialAccessManager;

/**
 * Checks access to editorial manager assignment pages.
 */
class AssignmentPagesAccessChecker implements AccessInterface {

  /**
   * Used to get entity from assignment form.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Used to check the entity type checked is supported by the module.
   *
   * @var \Drupal\editorial_access_manager\EditorialAccessManager
   */
  protected EditorialAccessManager $manager;

  /**
   * Constructs the access checker.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager service.
   * @param \Drupal\editorial_access_manager\EditorialAccessManager $manager
   *   Editorial access manager.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, EditorialAccessManager $manager) {
    $this->entityTypeManager = $entityTypeManager;
    $this->manager = $manager;
  }

  /**
   * Check access to editorial manager assignment page.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   Route match.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Account.
   * @param string $entity_type_id
   *   Entity type ID.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(RouteMatchInterface $route_match, AccountInterface $account, $entity_type_id) {
    if (!$this->manager->isSupported($entity_type_id) || !$this->isEditorialEnabledForEntity($route_match, $entity_type_id)) {
      return AccessResult::forbidden();
    }
    if (!empty($route_match->getParameter('entity_id')) && !empty($route_match->getParameter('langcode'))) {
      $entity = $this->entityTypeManager->getStorage($entity_type_id)->load($route_match->getParameter('entity_id'));
      if ($entity instanceof ContentEntityInterface) {
        return $this->accessForSpecificEntityAndLanguage($account, $entity, $entity_type_id, $route_match->getParameter('langcode'));
      }
    }
    return AccessResult::allowedIfHasPermissions($account, [
      'assign entity edition',
      'assign entity translation',
      sprintf('assign %s edition', $entity_type_id),
      sprintf('assign %s translation', $entity_type_id),
    ], 'OR');
  }

  /**
   * Check that editorial is enabled for the entity type.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   Route match.
   * @param string $entity_type_id
   *   Entity type ID.
   *
   * @return bool
   *   TRUE when the editorial is enabled for the entity type.
   */
  protected function isEditorialEnabledForEntity(RouteMatchInterface $route_match, string $entity_type_id) {
    $entity = $route_match->getParameter($entity_type_id);
    if (!$entity instanceof EntityInterface && !empty($route_match->getParameter('entity_id'))) {
      $entity = $this->entityTypeManager->getStorage($entity_type_id)->load($route_match->getParameter('entity_id'));
    }

    if ($entity instanceof EntityInterface) {
      $bundle_entity_type = (string) $entity->getEntityType()->getBundleEntityType();
      /** @var \Drupal\Core\Config\Entity\ConfigEntityBase $entity_type */
      $entity_type = $this->entityTypeManager->getStorage($bundle_entity_type)->load($entity->bundle());
      return $entity_type instanceof ThirdPartySettingsInterface && $entity_type->getThirdPartySetting('editorial_access_manager', 'enabled', FALSE);
    }
    return FALSE;
  }

  /**
   * Check access to assign the entity at specific language.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Account.
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   Entity.
   * @param string $entity_type_id
   *   Entity type ID.
   * @param string $langcode
   *   Language code.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   Access result letting know if user has access.
   */
  protected function accessForSpecificEntityAndLanguage(AccountInterface $account, ContentEntityInterface $entity, string $entity_type_id, string $langcode) {
    if ($entity->getUntranslated()->language()->getId() == $langcode) {
      $permissions = [
        sprintf('assign %s edition', $entity_type_id),
        'assign entity edition',
      ];
    }
    else {
      $permissions = [
        'assign entity translation',
        sprintf('assign %s translation', $entity_type_id),
      ];
    }
    return AccessResult::allowedIfHasPermissions($account, $permissions, 'OR');
  }

}
