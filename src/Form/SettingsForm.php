<?php

namespace Drupal\editorial_access_manager\Form;

use Drupal\Core\Config\ConfigBase;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Editorial access manager settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * Used to be able to specify which entity types will use the feature.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager */
    $entity_type_manager = $container->get('entity_type.manager');
    $instance->entityTypeManager = $entity_type_manager;
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'editorial_access_manager_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['editorial_access_manager.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('editorial_access_manager.settings');
    $entity_types_list = $this->getAllContentEntityTypesWithBundles();

    $form['entity_types'] = [
      '#required' => TRUE,
      '#type' => 'checkboxes',
      '#title' => $this->t('Entity types enabled'),
      '#description' => $this->t('Entity types'),
      '#options' => $entity_types_list,
      '#default_value' => $this->getEnabledEntityTypes($config, $entity_types_list),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * Get the list of entity types enabled.
   *
   * If there aren't taxonomy and node are default values
   * if the entities are present in the installlation (mostly probable).
   *
   * @param \Drupal\Core\Config\ConfigBase $config
   *   Current configuration of the module.
   * @param array $entity_types_list
   *   List of content entity types that are bundleable.
   *
   * @return array
   *   List of entity type ids.
   */
  protected function getEnabledEntityTypes(ConfigBase $config, array $entity_types_list) {
    $entity_types = $config->get('entity_types');
    return !empty($entity_types) ? array_keys(array_filter($config->get('entity_types'))) : $this->getDefaultValuesFromExistingEntityTypes($entity_types_list);
  }

  /**
   * Gets the list of content entity types that can have bundles.
   *
   * These are the only entity types allowed to use editorial
   * as usually only bundeable content entities are those
   * used to do content publishment in Drupal.
   *
   * @return array
   *   Key value list of content entity type ID as key and definition as value.
   */
  protected function getAllContentEntityTypesWithBundles() {
    $content_entity_types = [];
    $entity_type_definitions = $this->entityTypeManager->getDefinitions();
    foreach ($entity_type_definitions as $definition) {
      if (!empty($definition->getBundleOf()) && $this->entityTypeManager->getDefinition($definition->getBundleOf()) instanceof ContentEntityTypeInterface) {
        $content_entity_types[$definition->getBundleOf()] = $this->entityTypeManager->getDefinition($definition->getBundleOf())->getLabel();
      }
    }
    return $content_entity_types;
  }

  /**
   * List of default values for allowed entity types.
   *
   * The default values are different depending on the
   * enabled entity types on the site.
   *
   * @param array $entity_types
   *   List of available entity types.
   *
   * @return array
   *   List of default values.
   */
  protected function getDefaultValuesFromExistingEntityTypes(array $entity_types) {
    $default_values = [];
    foreach (['node', 'taxonomy_term'] as $default_entity_type_id) {
      if (isset($entity_types[$default_entity_type_id])) {
        $default_values[] = $default_entity_type_id;
      }
    }
    return $default_values;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('editorial_access_manager.settings');
    $entity_types_new = $form_state->getValue('entity_types');
    $old_entity_types_list = array_keys(array_filter($config->get('entity_types') ?? []));
    $config->set('entity_types', $entity_types_new)
      ->save();

    $new_entity_types_list = array_keys(array_filter($entity_types_new));
    if (in_array('node', array_merge($old_entity_types_list, $new_entity_types_list))
      && !in_array('node', array_intersect($new_entity_types_list, $old_entity_types_list))
    ) {
      node_access_rebuild(TRUE);
    }

    $this->messenger()->addWarning('To be able to view manage editorial access tabs it is needed to clear caches first.');

    parent::submitForm($form, $form_state);
  }

}
