# Editorial access manager

Editorial access manager allows the assignment to edit or creation of specific content (nodes, taxonomy terms...)
in specific languages to specific users.

Use this module in case you need to granularly restrict content edition and translation per language.

## How it works

1. Enable which content entity types are needed to support at /admin/config/content/editorial-access-manager
2. Enable the bundles that will use this feature at the specific bundle type edition (s.e.: node type edit).
3. Assign the permissions to assign users to edit to the respective roles. You can assign the 'Assign entity edition' and 'Assign entity translation',
or assign the specific entity type permission like 'Assign node edition' or 'Assign node translation'.
4. Assign the permissions to edit assigned content to the needed roles. It can be done generally or granularly,
for example granting the 'Assign entity edition' or granting 'Assign node edition'.

Please note that users that will edit or create content do not need to have any content creation /edition / translation permission, only
one of the permissions mentioned at point 4.

After that:

- Users allowed to assign content will have a tab in each content of the selected entity type and bundle named 'Manage editorial access', which will allow them to edit the form.
- Users allowed to edit assigned content will have a page under Content -> Assigned Content that will let them view which content have assigned, with the possibility to edit it, create
or edit translations.

### Reassigning content from one user to another

There are cases when assignments of a specific user must be transferred to another user.
There is a form to do that at /admin/content/reassign that let's you do that by selecting the old assignee and the new assignee. Only users with the "reassign assigned entities" permission will be able to do that.

## Contributing

Currently, all content entity types may be supported but it is recommended to declare the third party settings
of the contributed / custom entity types. An example can be seen at config/schema/editorial_access_manager.schema.yml.

In case it is needed to support a contributed content entity type feel free to open an issue to add it to the module.

