<?php

namespace Drupal\editorial_access_manager\Controller;

use Drupal\content_translation\Controller\ContentTranslationController;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Drupal\editorial_access_manager\EditorialAccessManager;
use Drupal\idn_user\Entity\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for Editorial access manager routes.
 */
class EditorialAccessManagerController extends ContentTranslationController {

  use LangcodeDetectionTrait;

  /**
   * Used to get the list of assignees of a specific language.
   *
   * @var \Drupal\editorial_access_manager\EditorialAccessManager
   */
  protected EditorialAccessManager $editorialAccessManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    /** @var \Drupal\editorial_access_manager\EditorialAccessManager $access_manager */
    $access_manager = $container->get('editorial_access_manager.manager');
    $instance->editorialAccessManager = $access_manager;
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function overview(RouteMatchInterface $route_match, $entity_type_id = NULL) {
    if (empty($entity_type_id)) {
      return [];
    }
    $entity = $route_match->getParameter($entity_type_id);

    if (!$entity instanceof ContentEntityInterface) {
      return [];
    }

    $build = parent::overview($route_match, $entity_type_id);

    $build['#title'] = $this->t('Editorial access management - @entity_label', ['@entity_label' => $entity->label()]);
    /**
     * @var int $delta
     * @var \Drupal\Core\StringTranslation\TranslatableMarkup $label
     */
    foreach ($build['content_translation_overview']['#header'] as $delta => $label) {
      if ($label->getUntranslatedString() == 'Operations') {
        $build['content_translation_overview']['#header'][$delta] = $this->t('Assigned editor');
        foreach ($build['content_translation_overview']['#rows'] as &$row) {
          $language = $this->detectLanguage($row[0], $entity);
          /** @var \Drupal\idn_user\Entity\UserInterface[] $assignees */
          $assignees = $this->editorialAccessManager->getEntityAssigneesPerLanguage($entity, $language->getId());
          if (!empty($assignees)) {
            $assignees = array_map(function (UserInterface $user) {
              return $user->label();
            }, $assignees);
          }
          else {
            $assignees = [];
          }
          $row[$delta] = [
            'data' => implode(', ', $assignees),
          ];

          if ($this->userHasAccessToAssignAtLanguage($entity, $language)) {
            $row[] = [
              'data' => [
                '#type' => 'link',
                '#url' => Url::fromRoute('editorial_access_manager.editorial_assignment', [
                  'entity_type_id' => $entity_type_id,
                  'entity_id' => $entity->id(),
                  'langcode' => $language->getId(),
                ], [
                  'query' => [
                    'destination' => Url::fromRoute((string) $route_match->getRouteName(), iterator_to_array($route_match->getRawParameters()->getIterator()))->toString(),
                  ],
                ]),
                '#attributes' => [
                  'class' => [
                    'use-ajax',
                  ],
                  'data-dialog-type' => 'dialog',
                  'data-dialog-options' => json_encode([
                    'width' => 400,
                  ]),
                ],
                '#title' => $this->t('Configure'),
              ],
            ];
          }
        }

      }
    }
    $build['#attached']['library'][] = 'core/jquery';
    $build['#attached']['library'][] = 'core/drupal.dialog.ajax';
    $build['#attached']['library'][] = 'core/jquery.form';
    return $build;
  }

  /**
   * Check ability to assign edit or translate at specific language.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   Entity access needs to be checked form.
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   Language of the entity.
   *
   * @return bool
   *   TRUE when the user has access to assign edition or translation.
   */
  protected function userHasAccessToAssignAtLanguage(ContentEntityInterface $entity, LanguageInterface $language) {
    if ($language->getId() != $entity->getUntranslated()->language()->getId()) {
      return $this->currentUser->hasPermission('assign entity translation')
        || $this->currentUser->hasPermission(sprintf('assign %s translation', $entity->getEntityTypeId()));
    }

    return $this->currentUser->hasPermission('assign entity edition')
      || $this->currentUser->hasPermission(sprintf('assign %s edition', $entity->getEntityTypeId()));
  }

}
