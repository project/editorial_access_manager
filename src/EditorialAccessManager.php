<?php

namespace Drupal\editorial_access_manager;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\Entity\ThirdPartySettingsInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\node\NodeGrantDatabaseStorageInterface;
use Drupal\node\NodeInterface;
use Drupal\user\UserInterface;

/**
 * Service description.
 */
class EditorialAccessManager {

  /**
   * Used to get entity from route parameters.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected RouteMatchInterface $routeMatch;

  /**
   * Used to load supported entity types.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Used to write and update the editorial access.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected Connection $database;

  /**
   * Module settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * Used to recalculate assigned node's grants.
   *
   * @var \Drupal\node\NodeGrantDatabaseStorageInterface
   */
  protected NodeGrantDatabaseStorageInterface $grantDatabaseStorage;

  /**
   * Constructs an EditorialAccessManager object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   * @param \Drupal\Core\Database\Connection $database
   *   Database service.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   Route match service.
   * @param \Drupal\node\NodeGrantDatabaseStorageInterface $grant_database_storage
   *   Node grants database storage service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entityTypeManager, Connection $database, RouteMatchInterface $route_match, NodeGrantDatabaseStorageInterface $grant_database_storage) {
    $this->config = $config_factory->get('editorial_access_manager.settings');
    $this->entityTypeManager = $entityTypeManager;
    $this->database = $database;
    $this->routeMatch = $route_match;
    $this->grantDatabaseStorage = $grant_database_storage;
  }

  /**
   * Check if a specific entity type is supported by the module.
   *
   * @param string $entity_type_id
   *   Entity type ID.
   *
   * @return bool
   *   TRUE if the entity type is supported (enabled in configuration).
   */
  public function isSupported(string $entity_type_id) {
    return in_array($entity_type_id, $this->getSupportedEntityTypesList());
  }

  /**
   * Get the list of supported entities.
   *
   * @return string[]
   *   Machine names of supported entities.
   */
  public function getSupportedEntityTypesList() {
    return array_keys(array_filter($this->config->get('entity_types') ?? []));
  }

  /**
   * Get the list of supported entities.
   *
   * @return array
   *   Key-valued list of entity type id and entity type definition.
   */
  public function getSupportedEntityTypes() {
    $supported_entity_types = [];
    foreach ($this->getSupportedEntityTypesList() as $entity_type_id) {
      $supported_entity_types[$entity_type_id] = $this->entityTypeManager->getDefinition($entity_type_id);
    }
    return $supported_entity_types;
  }

  /**
   * Gets the assignees of a specific entity and language.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity.
   * @param string $langcode
   *   Language code.
   *
   * @return \Drupal\user\UserInterface[]
   *   List of users.
   */
  public function getEntityAssigneesPerLanguage(EntityInterface $entity, string $langcode)  : array {
    $query = $this->database->select('editorial_access', 'ea')
      ->fields('ea', ['uid'])
      ->condition('ea.entity_type', (string) $entity->getEntityTypeId())
      ->condition('ea.entity_id', (string) $entity->id())
      ->condition('ea.langcode', $langcode);
    $query->leftJoin('users', 'u', 'u.uid = ea.uid');
    /** @var \Drupal\Core\Database\StatementInterface $query_result */
    $query_result = $query->execute();
    return array_filter(array_map(function ($uid) {
      return $this->entityTypeManager->getStorage('user')->load($uid);
    }, array_values($query_result->fetchAllKeyed(0, 0))), function ($user) {
      return $user instanceof UserInterface;
    });
  }

  /**
   * Clear all assignees for a specific entity and language.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity.
   * @param string $langcode
   *   Language.
   */
  public function clearEntityAssignees(EntityInterface $entity, string $langcode) {
    $this->database->delete('editorial_access')
      ->condition('entity_type', $entity->getEntityTypeId())
      ->condition('entity_id', (string) $entity->id())
      ->condition('langcode', $langcode)
      ->execute();
  }

  /**
   * Set all assignees for a specific entity and language.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity.
   * @param string $langcode
   *   Language.
   * @param array $assignees
   *   List of assignees.
   */
  public function setEntityAssignees(EntityInterface $entity, string $langcode, array $assignees) {
    $transaction = $this->database->startTransaction('editorial_access_manager_assignees');
    try {
      $this->clearEntityAssignees($entity, $langcode);

      foreach ($assignees as $assignee) {
        $this->database->insert('editorial_access')
          ->fields(['entity_type', 'entity_id', 'langcode', 'uid', 'date'], [
            $entity->getEntityTypeId(),
            $entity->id(),
            $langcode,
            $assignee,
            time(),
          ])
          ->execute();
      }
    }
    catch (\Exception $e) {
      watchdog_exception('editorial_access_manager', $e);
      $transaction->rollBack();
    }
  }

  /**
   * Gets an entity from route parameters.
   *
   * This method is used to manage access plus
   * user assignement.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   Entity.
   */
  public function getEntityFromRouteParameters() : ?EntityInterface {
    $entity_type = $this->routeMatch->getParameter('entity_type_id');
    $entity_id = $this->routeMatch->getParameter('entity_id');
    if (!empty($entity_type) && !empty($entity_id) && $this->entityTypeManager->hasDefinition($entity_type)) {
      return $this->entityTypeManager->getStorage($entity_type)->load($entity_id);
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getTranslationOverviewAccess(EntityInterface $entity, AccountInterface $user) {
    return $this->userIsAssignable($entity, $user)
      && $this->userIsAssignee($entity, $user);
  }

  /**
   * {@inheritdoc}
   */
  public function getTranslationAccess(EntityInterface $entity, AccountInterface $user, LanguageInterface $language) {
    return $this->userIsAssignable($entity, $user)
      && $this->userIsAssigneeAtLanguage($entity, $user, $language);
  }

  /**
   * Check if user is assignee at any of the languages of the entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Translatable entity.
   * @param \Drupal\Core\Session\AccountInterface $user
   *   User.
   *
   * @return bool
   *   TRUE when user is assignee of the entity in at least a language.
   */
  protected function userIsAssignee(EntityInterface $entity, AccountInterface $user) {
    /** @var \Drupal\Core\Database\StatementInterface $query_result */
    $query_result = $this->database->select('editorial_access', 'ea')
      ->fields('ea', ['uid'])
      ->condition('ea.entity_type', (string) $entity->getEntityTypeId())
      ->condition('ea.entity_id', (string) $entity->id())
      ->condition('ea.uid', (string) $user->id())
      ->execute();
    return !empty($query_result->fetchField());
  }

  /**
   * Check if user is assignee at a specific entity language.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Translatable entity.
   * @param \Drupal\Core\Session\AccountInterface $user
   *   User.
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   Entity language.
   *
   * @return bool
   *   TRUE when user is assignee of the entity in the specific language.
   */
  protected function userIsAssigneeAtLanguage(EntityInterface $entity, AccountInterface $user, LanguageInterface $language) {
    $assignees = $this->getEntityAssigneesPerLanguage($entity, $language->getId());
    foreach ($assignees as $assignee) {
      if ($user->id() == $assignee->id()) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Check if the entity edit / translation can be assigned.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity.
   * @param \Drupal\Core\Session\AccountInterface $user
   *   User.
   *
   * @return bool
   *   TRUE when the user has global or granular permissions
   *   to edit the assigned entity.
   */
  public function userIsAssignable(EntityInterface $entity, AccountInterface $user) {
    $bundle_entity_type = $entity->getEntityType()->getBundleEntityType();

    if (!empty($bundle_entity_type)) {
      $bundle_entity_type = $this->entityTypeManager->getStorage($bundle_entity_type)->load($entity->bundle());
      return ($bundle_entity_type instanceof ThirdPartySettingsInterface
          && $bundle_entity_type->getThirdPartySetting('editorial_access_manager', 'enabled', FALSE))
        && ($user->hasPermission('edit assigned entity')
          || $user->hasPermission(sprintf('edit assigned %s', $entity->getEntityTypeId())));
    }

    return FALSE;
  }

  /**
   * Recalculate grants of a specific node.
   *
   * Use this when after changing editorial access for a node.
   *
   * @param \Drupal\node\NodeInterface $node
   *   Node which grants will be recalculated.
   */
  public function recalculateNodeGrants(NodeInterface $node) {
    /** @var \Drupal\node\NodeAccessControlHandlerInterface $access_control_handler */
    $access_control_handler = $this->entityTypeManager->getAccessControlHandler('node');
    $grants = $access_control_handler->acquireGrants($node);
    $this->grantDatabaseStorage->write($node, $grants);
  }

}
